import { Component } from "@angular/core";
import { CoursesService } from "./courses.service";

@Component({
    selector:'courses',
    // template:`<h2>List of Courses</h2>
    //     <ul>
    //         <li *ngFor='let course of courses'> {{course}} </li>
    //     </ul>
    //     <!--<input type="text" [value]='email' (keyup.enter)="email=$event.target.value;onKeyUp()"/>-->
    //     <input type="text" [(ngModel)]="email" (keyup.enter)="onKeyUp()"/>
    //     <button class='btn btn-primary' [class.active]="isActive">Click Me</button>
    // `
    template:`<div class='ml-3'>
        {{ newCourse.title | uppercase }}<br/>
        {{ newCourse.rating | number:'1.1-1' }}<br/>
        {{ newCourse.students | number }}<br/>
        {{ newCourse.price | currency:'INR':true }}<br/>
        {{ newCourse.releaseDate | date:'shortDate' }}<br/>
        <h3>Custom Content</h3>
        {{longText | summary:10}}
        </div>
    `
})
export class CoursesComponent{
    courses:string[];
    isActive = true;

    email='james@bond.com'
    constructor(service:CoursesService){
        this.courses = service.courses;
    }

    onKeyUp(){
        console.log(this.email)

    }

    newCourse = {
        title:"Angular Masterclass Basic to Advance",
        rating:4.9745,
        students:30123,
        price:190.95,
        releaseDate:new Date(2016, 3, 1)
    }

    longText=`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc gravida, neque eu aliquam porttitor, elit velit varius dui, et porttitor elit justo condimentum magna. Mauris felis quam, interdum at ex quis, lobortis pharetra dui. Nullam lacus est, mattis vel tellus sit amet, lacinia malesuada elit. Aliquam quis dapibus ipsum, a malesuada eros. Nunc eleifend leo sit amet sodales volutpat. Aenean ut augue vitae leo semper tempor nec tempus nisi. Donec interdum ullamcorper leo, eu ultricies ex ornare non. In molestie eu augue sed convallis. Curabitur egestas finibus scelerisque. In ac lacus lectus. Nunc risus tortor, malesuada eget dapibus vitae, facilisis et velit. Phasellus lacinia sit amet nisl sit amet imperdiet. Donec consequat neque vitae justo congue, nec efficitur lectus dictum. Ut tempor urna id eros molestie dictum.
    Duis vel faucibus enim. Sed pretium condimentum tristique. Mauris rutrum massa eget est elementum luctus. Nulla nec dolor orci. Nam sit amet posuere tellus. Duis et pretium libero, lobortis ornare dui. Nunc leo metus, lobortis vitae lobortis eget, fermentum vitae enim. Sed leo neque, lobortis ut convallis non, pellentesque et massa. Phasellus at augue ac arcu aliquam varius non sit amet velit. Mauris molestie consequat enim a tincidunt. Nam pharetra arcu convallis, venenatis nisl non, tristique mauris. Sed sed nisi a purus pellentesque condimentum. Vestibulum egestas eget ligula eget interdum.
    Nulla urna mi, vulputate et ipsum in, congue efficitur turpis. Aliquam rhoncus, urna sit amet tempor varius, ipsum nisl dapibus urna, ac fringilla eros erat vitae nisl. Curabitur rutrum dui orci, in commodo odio lobortis quis. Fusce in sapien urna. Mauris laoreet purus ac sapien mollis lacinia. Curabitur ut ultrices elit. Donec vitae cursus metus, a dignissim nunc. Sed tincidunt, ligula at venenatis eleifend, tellus ante cursus libero, sed pulvinar libero mi a elit. Curabitur semper, erat id porttitor pretium, purus nisl varius magna, eu aliquet nibh ante a elit. Curabitur eu ultrices augue.
    Morbi in sem in nisi blandit venenatis eu sit amet metus. Sed ultricies augue nec libero pharetra, sit amet mollis ipsum porttitor. Praesent condimentum fringilla nisi mollis convallis. Aliquam tincidunt sodales felis ullamcorper ultrices. Nunc elementum placerat facilisis. Pellentesque accumsan ullamcorper sem, ac iaculis eros laoreet eget. Pellentesque lorem nisl, volutpat eu ultrices et, tincidunt sit amet lacus. Cras lectus leo, convallis et felis vel, interdum dignissim leo. Aenean aliquam est nec lectus finibus, at facilisis eros imperdiet. Proin efficitur aliquet odio, sit amet eleifend ligula condimentum sit amet. Nunc scelerisque lacus non faucibus placerat. Mauris eget mi in massa maximus accumsan. Aenean eu justo vitae nisi interdum tincidunt at et lacus. Duis sit amet diam lorem. Nunc pharetra feugiat maximus. Nullam in cursus odio.
    Ut et mi ex. Integer varius dolor eu quam euismod, eu consectetur nisl varius. Maecenas quis luctus nibh. Aenean in sodales neque. Integer et rhoncus dui, eu consectetur sapien. Suspendisse pellentesque nunc quis ipsum pharetra facilisis. In ultrices mauris sed justo ullamcorper condimentum. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; Mauris mollis, risus non mollis hendrerit, mi ligula tempor erat, eu hendrerit nisi metus mollis lacus. Aenean tincidunt rutrum enim non aliquet.`
}